#include <DHT.h>                             

#define DHTPIN 2                      
#define DHTTYPE DHT11 
DHT dht(DHTPIN, DHTTYPE);    

float hum;                                    
float temp;     
 // "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillis = 0;  // will store last time temp/hum was updated

const long interval = 2000;  // interval at which to screen data (2 sec)                                  

void setup()
{
  Serial.begin(9600);                         
  dht.begin(); 
                            
}

void loop()
{  
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval) {
    
    previousMillis = currentMillis;
  hum = dht.readHumidity();                 
  temp = dht.readTemperature();                
  
  if (isnan(hum) || isnan(temp))             
  {    
    Serial.println("Chyba cteni!");          
  }
  
  else                                       
  {                    
    Serial.print(hum);
    Serial.print(";");
    Serial.print(temp);
    Serial.println(";");
  }
     
  }                          
}
