import React from "react";
import UU5 from "uu5g04";
import { Client } from "uu_appg01";
import { useRef, useData, createVisualComponent, createComponent } from "uu5g04-hooks";

const BINARY_URL = "https://uuapp.plus4u.net/uu-bookkit-maing01/5c73a1fdb9a14b4aaff232962752c9b6/getBinaryData";

const classNames = {
  main: ({ selected }) => UU5.Common.Css.css`
      height: 100%;
      border: 1px solid ${selected ? `#FF9800` : `rgb(189, 189, 189)`};
      overflow: hidden;
      border-radius: 4px;
      display: flex;
      flex-direction: column;
    `,
  divImage: (type) => UU5.Common.Css.css`
      position: relative;
      height: ${type === "image" ? `100%` : `auto`};
      min-height: 200px;
      overflow: hidden;
      background: "#f5f5f5";
      flex: none;
    `,
  image: UU5.Common.Css.css`
      width: 100%;
      position: absolute;
      top: -50%;
      left: 0;
      right: 0;
      bottom: -50%;
      margin: auto;
    `,
  divText: (type) => UU5.Common.Css.css`
      position: relative;
      width: 100%;
      height: ${type === "image" ? `100%` : `auto`};
      overflow: hidden;
      padding: 8px;
      display: flex;
      flex-direction: column;
      flex-grow: 1;
  `,
};

function streamToString(stream, encoding = "utf-8") {
  return window.TextDecoder
    ? new window.TextDecoder(encoding).decode(stream)
    : decodeURIComponent(escape(stream.map((char) => String.fromCharCode(char)).join("")));
}

async function onLoad(dtoIn) {
  let response = await Client.get(BINARY_URL, { code: "basicExampleData" });
  let data = response.data instanceof Uint8Array ? JSON.parse(streamToString(response.data)) : response.data;
  return data;
}

export const AuthnAnimalLoader = createComponent({
  render(props) {
    return (
      <UU5.Common.Identity>
        {({ identity, login, logout, ...opt }) => {
          return identity ? (
            <AnimalLoader {...props} />
          ) : identity === null ? (
            <UU5.Bricks.Button onClick={() => login()} content="Log in" />
          ) : null;
        }}
      </UU5.Common.Identity>
    );
  },
});

export const AnimalLoader = createComponent({
  //@@viewOn:statics
  displayName: "UU5.Tiles.Demo.AnimalLoader",
  //@@viewOff: statics

  //@@viewOn:propTypes
  propTypes: {},
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {},
  //@@viewOff:defaultProps

  render({ children }) {
    //@@viewOn:hooks
    let { asyncData, viewState, error } = useData({ onLoad });
    //@@viewOff:hooks

    //@@viewOn:interface
    //@@viewOff:interface

    //@@viewOn:private
    //@@viewOff:private

    //@@viewOn:render
    return viewState === "ready" ? (
      children({ data: asyncData })
    ) : viewState === "error" ? (
      <UU5.Common.Error error={error} moreInfo />
    ) : (
      <UU5.Bricks.Loading />
    );
    //@@viewOff:render
  },
});

export const AnimalTile = createVisualComponent({
  //@@viewOn:statics
  displayName: "UU5.Tiles.Demo.AnimalTile",
  nestingLevel: "box",
  //@@viewOff: statics

  //@@viewOn:propTypes
  propTypes: {
    type: UU5.PropTypes.oneOf(["full", "image", "small"]),
  },
  //@@viewOff:propTypes

  //@@viewOn:getDefaultProps
  defaultProps: {
    type: "full",
  },
  //@@viewOff:getDefaultProps

  render(props) {
    let { data, type, handleMeasure } = props;
    if (data && data.data) data = data.data; // to be able to work with useDataList as well as with older usePagingListData

    //@@viewOn:hooks
    let imageRef = useRef();
    //@@viewOff:hooks

    //@@viewOn:interface
    //@@viewOff:interface

    //@@viewOn:private
    function onLoadImage() {
      if (typeof handleMeasure === "function") handleMeasure();
    }
    //@@viewOff:private

    let mainAttrs = UU5.Common.VisualComponent.getAttrs(props);
    mainAttrs.className = (mainAttrs.className ? mainAttrs.className + " " : "") + classNames.main(props);

    //@@viewOn:render
    return (
      <div {...mainAttrs}>
        {(type === "full" || type === "image") && (
          <div className={classNames.divImage(type)}>
            <UU5.Bricks.Image
              src={BINARY_URL + "?code=" + data.img}
              alt={data.img}
              responsive={false}
              className={classNames.image}
              mainAttrs={{ onLoad: onLoadImage }}
              ref_={imageRef}
              authenticate
            />
          </div>
        )}
        {type !== "image" && (
          <div className={classNames.divText(type)}>
            <div className={UU5.Common.Css.css`padding-bottom: 8px`}>
              <UU5.Bricks.Icon icon="mdi-paw" className={UU5.Common.Css.css`margin-right: 8px`} />
              <strong className={UU5.Common.Css.css`margin-right: 8px`}>
                <UU5.Bricks.Lsi lsi={data.speciesName} />
              </strong>
              <em>
                <small>({data.speciesTaxonomyName})</small>
              </em>
            </div>
            {type === "full" && [
              <UU5.Bricks.Text key="location" colorSchema="green">
                <UU5.Bricks.Icon icon="mdi-earth" className={UU5.Common.Css.css`margin-right: 8px`} />
                <UU5.Bricks.Lsi lsi={data.location} />
              </UU5.Bricks.Text>,
              <div key="description" className={UU5.Common.Css.css`flex-grow: 2; font-size: s`}></div>,
              <small key="taxonomy">
                <div>
                  <UU5.Bricks.Lsi lsi={{ en: "Class", cs: "Třída" }} />:{"\xA0"}
                  <strong>
                    <UU5.Bricks.Lsi lsi={data.class1} />
                  </strong>
                  , <UU5.Bricks.Lsi lsi={{ en: "Order", cs: "Řád" }} />:{"\xA0"}
                  <strong>
                    <UU5.Bricks.Lsi lsi={data.order} />
                  </strong>
                  , <UU5.Bricks.Lsi lsi={{ en: "Family", cs: "Čeleď" }} />:{"\xA0"}
                  <strong>
                    <UU5.Bricks.Lsi lsi={data.family} />
                  </strong>
                </div>
              </small>,
            ]}
          </div>
        )}
      </div>
    );
    //@@viewOff:render
  },
});

export default AnimalTile;
