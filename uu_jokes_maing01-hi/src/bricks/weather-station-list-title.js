//@@viewOn:imports
import { createVisualComponent, Lsi, useRoute } from "uu5g05";
import Uu5Elements from "uu5g05-elements";
import Config from "./config/config.js";
import WeatherStationCfg from "../config/weatherStation";
import { usePermission } from "../hooks/permission-context.js";

//@@viewOff:imports

//@@viewOn:constants
//@@viewOff:constants

//@@viewOn:css
const Css = {
  main: () => Config.Css.css({}),
};
//@@viewOff:css

//@@viewOn:helpers
//@@viewOff:helpers

const WeatherStationListTitle = createVisualComponent({
  //@@viewOn:statics
  uu5Tag: Config.TAG + "WeatherStationListTitle",
  nestingLevel: ["bigBoxCollection", "bigBox"],
  //@@viewOff:statics

  //@@viewOn:propTypes
  propTypes: {},
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {},
  //@@viewOff:defaultProps

  render(props) {
    //@@viewOn:private
    const [, setRoute] = useRoute();
    const permission = usePermission();
    //@@viewOff:private

    //@@viewOn:interface
    //@@viewOff:interface

    //@@viewOn:render
    return (
      <Uu5Elements.Block
        card={"full"}
        header={props.data.data.name}
        footer={props.footer}
        collapsible={false}
        disabled={props.data.state === "pending"}
        actionList={
          props.data.state === "pending"
            ? [
                {
                  icon: "mdi-loading mdi-spin",
                },
              ]
            : [
                {
                  icon: "mdi-file-document-outline",
                  collapsedChildren: <Lsi lsi={WeatherStationCfg.tile.open} />,
                  onClick: () => setRoute("dashboard", { id: props.data.data.id, name: props.data.data.name }),
                },
                permission.station.canUpdate(props.data.data) && {
                  icon: "mdi-pencil",
                  colorScheme: "blue",
                  collapsedChildren: <Lsi lsi={WeatherStationCfg.tile.update} />,
                  onClick: () => props.setSelectedSubject(props.data),
                },
                permission.station.canDelete(props.data.data) && {
                  icon: "mdi-trash-can-outline",
                  colorScheme: "red",
                  collapsedChildren: <Lsi lsi={WeatherStationCfg.tile.delete} />,
                  onClick: () => props.setSubjectToDelete(props.data),
                },
              ]
        }
      >
        <ul>
          <li>{props.data.data.code}</li>
          <li>{props.data.data.location}</li>
        </ul>
      </Uu5Elements.Block>
    );
    //@@viewOff:render
  },
});

//@@viewOn:exports
export { WeatherStationListTitle };
export default WeatherStationListTitle;
//@@viewOff:exports
