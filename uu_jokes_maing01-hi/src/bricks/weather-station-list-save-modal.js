//@@viewOn:imports
import UU5 from "uu5g04";
import { createComponent, useCallback, useState } from "uu5g05";
import Config from "./config/config.js";
//@@viewOff:imports

//@@viewOn:constants
//@@viewOff:constants

//@@viewOn:helpers
//@@viewOff:helpers

const WeatherStationListSaveModal = createComponent({
  //@@viewOn:statics
  uu5Tag: Config.TAG + "WeatherStationListSaveModal",
  //@@viewOff:statics

  //@@viewOn:propTypes
  propTypes: {
    onClose: UU5.PropTypes.func,
    onCreate: UU5.PropTypes.func,
  },
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {
    onClose: undefined,
    onCreate: undefined,
  },
  //@@viewOff:defaultProps

  render({ onClose, onCreate }) {
    //@@viewOn:private
    const [viewName, setViewName] = useState("");
    const onBlur = useCallback((opt) => {
      setViewName(opt.value);
      opt.component.onBlurDefault(opt);
    }, []);
    const usedOnCreate = useCallback(() => {
      if (typeof onCreate === "function") {
        onCreate(viewName);
      }
    }, [onCreate, viewName]);
    //@@viewOff:private

    //@@viewOn:interface
    //@@viewOff:interface

    //@@viewOn:render
    return (
      <UU5.Forms.ContextModal
        shown
        location="portal"
        header={
          <UU5.Forms.ContextHeader
            content={<UU5.Bricks.Lsi lsi={{ en: "Create a system view" }} />}
            info={
              <UU5.Bricks.Lsi
                lsi={{
                  en: `This modal allows to create a new "system" view from the current set of columns, filters and sorters. This view can't be removed in the ViewBar component. Define the view's name and press "Save".`,
                }}
              />
            }
          />
        }
        footer={
          <UU5.Forms.ContextControls
            buttonSubmitProps={{
              content: <UU5.Bricks.Lsi lsi={{ en: "Save" }} />,
            }}
            buttonCancelProps={{
              content: <UU5.Bricks.Lsi lsi={{ en: "Cancel" }} />,
            }}
          />
        }
        onClose={onClose}
      >
        <UU5.Forms.ContextForm onSave={usedOnCreate} onCancel={onClose}>
          <UU5.Forms.Text onBlur={onBlur} label="view's name" />
        </UU5.Forms.ContextForm>
      </UU5.Forms.ContextModal>
    );
    //@@viewOff:render
  },
});

//@@viewOn:exports
export { WeatherStationListSaveModal };
export default WeatherStationListSaveModal;
//@@viewOff:exports
