//@@viewOn:imports
import { createVisualComponent, Lsi, useDataList, useMemo } from "uu5g05";
import Uu5Forms from "uu5g05-forms";
import Uu5Elements from "uu5g05-elements";
import { useController } from "uu5tilesg02";
import Config from "./config/config.js";
import WeatherStationCfg from "../config/weatherStation";
import Calls from "calls";
import { assocPath, keys, lensProp, over, pipe, reduce, values } from "ramda";
import { useTranslateConst } from "../hooks/useTranslateConst.js";
import { DATA_TYPES, EQUIPMENT } from "../constants/common.js";
//@@viewOff:imports

//@@viewOn:constants
//@@viewOff:constants

//@@viewOn:css
const Css = {
  main: () => Config.Css.css({}),
};
//@@viewOff:css

//@@viewOn:helpers
//@@viewOff:helpers

const WeatherStationForm = createVisualComponent({
  //@@viewOn:statics
  uu5Tag: Config.TAG + "WeatherStationForm",
  //@@viewOff:statics

  //@@viewOn:propTypes
  propTypes: {},
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {},
  //@@viewOff:defaultProps

  render(props) {
    //@@viewOn:private
    const tilesController = useController();
    const { translateConstForSelect } = useTranslateConst();

    const studyProgramDataList = useDataList({
      handlerMap: {
        load: Calls.listStudyPrograms,
      },
      initialDtoIn: {},
    });

    const categoryList = useMemo(() => {
      const result = [];
      studyProgramDataList.data?.forEach((item) => result.push({ value: item.data.id, children: item.data.name }));
      return result;
    }, [studyProgramDataList.data]);
    //@@viewOff:private

    //@@viewOn:interface
    //@@viewOff:interface

    //@@viewOn:render
    return (
      <Uu5Forms.Form.Provider
        onSubmit={async (e) => {
          let newStation;
          
          if (props.selectedSubject.data) {
            const updateItem = props.selectedSubject?.handlerMap?.updateItem || props.updateItem;
            newStation = await updateItem({
              id: props.selectedSubject.data.id,
              ...e.data.value,
            });
          } else {
            newStation = await props.createItem(e.data.value);
          }
          props.setSelectedSubject(null);
        }}
      >
        <Uu5Elements.Modal
          header={<Lsi lsi={props.selectedSubject.data ? WeatherStationCfg.update : WeatherStationCfg.create} />}
          footer={
            <div className={Config.Css.css({ display: "flex", gap: 8, justifyContent: "flex-end" })}>
              <Uu5Forms.CancelButton onClick={() => props.setSelectedSubject(null)} />
              <Uu5Forms.SubmitButton />
            </div>
          }
          open={true}
          onClose={() => props.setSelectedSubject(null)}
        >
          <Uu5Forms.Form.View>
            <Uu5Forms.FormText
              label={<Lsi lsi={WeatherStationCfg.form.name} />}
              name={"name"}
              initialValue={props.selectedSubject.data?.name}
              required
            />
            <Uu5Forms.FormText
              label={<Lsi lsi={WeatherStationCfg.form.code} />}
              name={"code"}
              initialValue={props.selectedSubject.data?.code}
              required
            />
            <Uu5Forms.FormText
              label={<Lsi lsi={WeatherStationCfg.form.serialNumber} />}
              name={"serialNumber"}
              initialValue={props.selectedSubject.data?.serialNumber}
              required
            />
            <Uu5Forms.FormText
              label={<Lsi lsi={WeatherStationCfg.form.location} />}
              name={"location"}
              initialValue={props.selectedSubject.data?.location}
              required
            />
            <Uu5Forms.FormTextSelect
              label={<Lsi lsi={WeatherStationCfg.form.equipment} />}
              name={"equipment"}
              itemList={translateConstForSelect(EQUIPMENT)}
              initialValue={props.selectedSubject.data?.equipment}
              multiple
              required
            />
            <Uu5Forms.FormTextSelect
              label={<Lsi lsi={WeatherStationCfg.form.dataTypes} />}
              name={"dataTypes"}
              itemList={translateConstForSelect(DATA_TYPES)}
              initialValue={props.selectedSubject.data?.dataTypes}
              multiple
              required
            />

            <Uu5Forms.FormCheckbox
              label={<Lsi lsi={WeatherStationCfg.form.isPublic} />}
              name={"isPublic"}
              initialValue={props.selectedSubject.data?.isPublic}
            />
          </Uu5Forms.Form.View>
        </Uu5Elements.Modal>
      </Uu5Forms.Form.Provider>
    );
    //@@viewOff:render
  },
});

//@@viewOn:exports
export { WeatherStationForm };
export default WeatherStationForm;
//@@viewOff:exports
