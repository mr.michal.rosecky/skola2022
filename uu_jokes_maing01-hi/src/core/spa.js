//@@viewOn:imports
import { createVisualComponent, withLazy } from "uu5g05";
import Uu5Elements from "uu5g05-elements";
import Plus4U5 from "uu_plus4u5g02";
import Plus4U5App from "uu_plus4u5g02-app";

import Config from "./config/config.js";
import Home from "../routes/home.js";
import PermissionProvider from "../hooks/permission-provider.js";
import RouteBar from "./route-bar.js";
//@@viewOff:imports

//@@viewOn:constants
const About = withLazy(() => import("../routes/about.js"), <Plus4U5App.SpaPending />);
const WeatherStationList = withLazy(() => import("../routes/weather-station-list"), <Plus4U5App.SpaPending />);
const Dashboard = withLazy(() => import("../routes/dashboard.js"), <Plus4U5App.SpaPending />);
const InitAppWorkspace = withLazy(() => import("../routes/init-app-workspace.js"), <Plus4U5App.SpaPending />);
const ControlPanel = withLazy(() => import("../routes/control-panel.js"), <Plus4U5App.SpaPending />);

const ROUTE_MAP = {
  "": { redirect: "home" },
  home: (props) => <Home {...props} />,
  about: (props) => <About {...props} />,
  "weather-station-list": (props) => <WeatherStationList {...props} />,
  "dashboard": (props) => <Dashboard {...props} />,
  "sys/uuAppWorkspace/initUve": (props) => <InitAppWorkspace {...props} />,
  controlPanel: (props) => <ControlPanel {...props} />,
  "*": () => (
    <Uu5Elements.Text category="story" segment="heading" type="h1">
      Not Found
    </Uu5Elements.Text>
  ),
};
//@@viewOff:constants

//@@viewOn:css
//@@viewOff:css

//@@viewOn:helpers
//@@viewOff:helpers

const Spa = createVisualComponent({
  //@@viewOn:statics
  uu5Tag: Config.TAG + "Spa",
  //@@viewOff:statics

  //@@viewOn:propTypes
  propTypes: {},
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {},
  //@@viewOff:defaultProps

  render() {
    //@@viewOn:private
    //@@viewOff:private

    //@@viewOn:interface
    //@@viewOff:interface

    //@@viewOn:render
    return (
      <Plus4U5.SpaProvider initialLanguageList={["en", "cs"]}>
        <Uu5Elements.ModalBus>
          <PermissionProvider>
            <Plus4U5App.Spa>
              <RouteBar />
              <Plus4U5App.Router routeMap={ROUTE_MAP} />
            </Plus4U5App.Spa>
          </PermissionProvider>
        </Uu5Elements.ModalBus>
      </Plus4U5.SpaProvider>
    );
    //@@viewOff:render
  },
});

//@@viewOn:exports
export { Spa };
export default Spa;
//@@viewOff:exports
