import { path, split } from "ramda";
import { Lsi, useCallback, useLsiValues } from "uu5g05";
import cfg from "../config/index.js";

export const useTranslateConst = () => {
  const translateConstForSelect = (constant) => {
    return Object.keys(constant).map((key) => {
      const pathArray = split(".", constant[key]);
      const lsi = path(pathArray, cfg);
      return { value: key, children: <Lsi lsi={lsi} /> };
    });
  };
  const translateConstByKey = useCallback((constant, key, defaultString = "") => {
    if (constant[key]) {
      const pathArray = split(".", constant[key]);
      const lsi = path(pathArray, cfg);
      return <Lsi lsi={lsi} />;
    }
    return defaultString;
  });
  return {
    translateConstForSelect,
    translateConstByKey,
  };
};
