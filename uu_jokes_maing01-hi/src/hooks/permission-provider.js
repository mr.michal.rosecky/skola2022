//@@viewOn:imports
import { createComponent, useDataObject, useMemo, useSession } from "uu5g05";
import Config from "../config/config";
import PermissionContext from "./permission-context";
import Calls from "../calls";

//@@viewOff:imports

const Pre = createComponent({
  //@@viewOn:statics
  uu5Tag: Config.TAG + "Pre",
  //@@viewOff:statics

  //@@viewOn:propTypes
  propTypes: {},
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {},
  //@@viewOff:defaultProps

  render(props) {
    //@@viewOn:private
    const { identity } = useSession();
    //@@viewOff:private

    //@@viewOn:render
    if (identity === undefined) {
      return <UU5.Bricks.Loading />;
    }
    return <PermissionProvider {...props} />;
    //@@viewOff:render
  },
});

export const PermissionProvider = createComponent({
  //@@viewOn:statics
  uu5Tag: Config.TAG + "PermissionProvider",
  //@@viewOff:statics

  //@@viewOn:propTypes
  propTypes: {},
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {},
  //@@viewOff:defaultProps

  render(props) {
    //@@viewOn:private
    const { identity } = useSession();

    const { data } = useDataObject({
      handlerMap: {
        load: Calls.permission,
      },
      initialDtoIn: {},
    });

    const permission = useMemo(() => {
      const profileList = data?.sysData?.profileData?.uuIdentityProfileList || [];
      // const isAuthority = true;
      const isPublic = profileList.length === 0;
      const isAuthority = profileList.includes("Authorities");
      const isExecutive = profileList.includes("Executives");
      const isAwidLicenseOwner = profileList.includes("AwidLicenseOwner");

      function isOwner(entity) {
        return identity?.uuIdentity === entity.uuIdentity;
      }

      const station = {
        canCreate: () => isAuthority,
        canUpdate: () => isAuthority,
        canDelete: () => isAuthority,
        canUpdateContent: () => isAuthority,
        canShowList: () => isAuthority || isExecutive || isPublic,
      };

      return {
        station,
      };
    }, [data, identity]);
    //@@viewOff:private

    //@@viewOn:render
    return (
      <PermissionContext.Provider value={permission}>
        {typeof props.children === "function" ? props.children(permission) : props.children}
      </PermissionContext.Provider>
    );
    //@@viewOff:render
  },
});

export default Pre;
