//@@viewOn:imports
import {
  Utils,
  createVisualComponent,
  Lsi,
  useState,
  useRoute,
  useRef,
  useScreenSize,
  useDataList,
  useMemo,
} from "uu5g05";
import Uu5Elements from "uu5g05-elements";
import { withRoute } from "uu_plus4u5g02-app";
import HomeCfg from "../config/home.js";
import Config from "./config/config.js";
import LSI from "../config/lsi.js";
import Calls from "../calls";
import Uu5Tiles from "uu5tilesg02";

//@@viewOff:imports

//@@viewOn:constants
//@@viewOff:constants

//@@viewOn:css
const Css = {
  icon: () =>
    Config.Css.css({
      fontSize: 48,
      lineHeight: "1em",
    }),
};
//@@viewOff:css

//@@viewOn:helpers
//@@viewOff:helpers

let Home = createVisualComponent({
  //@@viewOn:statics
  uu5Tag: Config.TAG + "Home",
  //@@viewOff:statics

  //@@viewOn:propTypes
  propTypes: {},
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {},
  //@@viewOff:defaultProps

  render(props) {
    //@@viewOn:private
    const [viewType, setViewType] = useState("grid");
    const [, setRoute] = useRoute();
    const tilesProviderRef = useRef();
    const screensize = useScreenSize();

    const studyProgramDataList = useDataList({
      handlerMap: {
        load: Calls.listStudyPrograms,
      },
      initialDtoIn: {},
    });
    //@@viewOff:private

    //@@viewOn:interface
    //@@viewOff:interface

    //@@viewOn:render
    const attrs = Utils.VisualComponent.getAttrs(props);
    return (
      <div {...attrs}>
        <UU5.Bricks.Container>
          <UU5.BlockLayout.Tile margin="10px 0 0" style={{ backgroundColor: "lightBlue" }}>
            <div className={Config.Css.css({ margin: "16px 32px" })}>
              <Uu5Elements.Block
                header={
                  <Uu5Elements.Text category="story" segment="heading" type="h3">
                    <Lsi lsi={HomeCfg.welcome} />
                  </Uu5Elements.Text>
                }
              >
                <UU5.BlockLayout.Line />
              </Uu5Elements.Block>
              <Uu5Elements.Block
                header={
                  <Uu5Elements.Text category="story" segment="heading" type="h5">
                    <Lsi lsi={HomeCfg.header} />
                  </Uu5Elements.Text>
                }
              >
                <div>
                  <Lsi lsi={HomeCfg.info.study} />
                </div>
              </Uu5Elements.Block>
              <UU5.Bricks.Row display="flex">
                <UU5.Bricks.Column
                  colWidth="xs-12 s-6 m-4"
                  style={{ backgroundColor: "lightYellow" }}
                  className="uu5-common-padding-xl"
                >
                  <Uu5Elements.Block
                    header={
                      <Uu5Elements.Text category="story" segment="heading" type="h2">
                        {/*                                 <UU5.Bricks.Link href="https://unicornuniversity.net/en/study-forms" target="_blank"> */}
                        {/*                                 <div><Lsi lsi={HomeCfg.fullTime.header} /></div> */}
                        {/*                                 </UU5.Bricks.Link> */}
                      </Uu5Elements.Text>
                    }
                  >
                    <div>
                      <Lsi lsi={HomeCfg.fullTime.body} />
                    </div>
                  </Uu5Elements.Block>
                </UU5.Bricks.Column>

                <UU5.Bricks.Column
                  colWidth="xs-12 s-6 m-4"
                  style={{ backgroundColor: "lightYellow" }}
                  className="uu5-common-padding-xl"
                >
                  <Uu5Elements.Block
                    header={<Uu5Elements.Text category="story" segment="heading" type="h2"></Uu5Elements.Text>}
                  >
                    <div>
                      <Lsi lsi={HomeCfg.partTime.body} />
                    </div>
                  </Uu5Elements.Block>
                </UU5.Bricks.Column>

                <UU5.Bricks.Column
                  colWidth="xs-12 s-6 m-4"
                  style={{ backgroundColor: "lightYellow" }}
                  className="uu5-common-padding-xl"
                >
                  <Uu5Elements.Block
                    header={
                      <Uu5Elements.Text category="story" segment="heading" type="h2">
                        {/*                       <UU5.Bricks.Link href="https://unicornuniversity.net/en/study-forms" target="_blank"> */}
                        {/*                       <div><Lsi lsi={HomeCfg.online.header} /></div> */}
                        {/*                     </UU5.Bricks.Link> */}
                      </Uu5Elements.Text>
                    }
                  >
                    <div>
                      <Lsi lsi={HomeCfg.online.body} />
                    </div>
                  </Uu5Elements.Block>
                </UU5.Bricks.Column>
              </UU5.Bricks.Row>
            </div>
          </UU5.BlockLayout.Tile>
        </UU5.Bricks.Container>

        <UU5.Bricks.Container>
          <UU5.BlockLayout.Tile margin="10px 0 0" style={{ backgroundColor: "lightBlue" }}>
            <div className={Config.Css.css({ margin: "16px 32px" })}>
              <UU5.Bricks.Column colWidth="xs-12 s-8" style={{ backgroundColor: "none" }}>
                <Uu5Elements.Block
                  header={
                    <Uu5Elements.Text category="story" segment="heading" type="h3">
                      <Lsi lsi={HomeCfg.videa.header} />
                    </Uu5Elements.Text>
                  }
                >
                  <UU5.BlockLayout.Line />
                </Uu5Elements.Block>
              </UU5.Bricks.Column>
              <UU5.Bricks.Row className={Config.Css.css({ margin: "20px 10px 100px 100px" })}>
                <UU5.Bricks.Column height="284" width="504" style={{ backgroundColor: "white" }}>
                  <iframe
                    width="560"
                    height="315"
                    src="https://www.youtube.com/embed/dMKIrjPFK-8"
                    title="YouTube video player"
                    frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                    allowfullscreen
                  ></iframe>
                </UU5.Bricks.Column>
                <UU5.Bricks.Column height="284" width="504" style={{ backgroundColor: "white" }}>
                  {/*     <iframe src="https://www.linkedin.com/embed/feed/update/urn:li:ugcPost:6914925280952209410?compact=1" height="284" width="504" frameborder="0" allowfullscreen="" title="Embedded post"></iframe> */}
                </UU5.Bricks.Column>
                {/*  <div className={Config.Css.css({ margin: "160px" })}></div> */}
              </UU5.Bricks.Row>
            </div>
          </UU5.BlockLayout.Tile>
        </UU5.Bricks.Container>
      </div>
    );
    //@@viewOff:render
  },
});

Home = withRoute(Home, { authenticated: false });

//@@viewOn:exports
export { Home };
export default Home;
//@@viewOff:exports
