//@@viewOn:imports
import { createVisualComponent, Lsi, useDataList, useRef, useScreenSize, useState, Utils } from "uu5g05";
import { withRoute } from "uu_plus4u5g02-app";
import Uu5Tiles from "uu5tilesg02";
import Calls from "calls";

import Config from "./config/config.js";
import WeatherStationCfg from "../config/weatherStation.js";
import { usePermission } from "../hooks/permission-context.js";
import Uu5Elements from "uu5g05-elements";
import LSI from "../config/lsi.js";
import WeatherStationListTitle from "../bricks/weather-station-list-title.js";
import WeatherStationForm from "../bricks/weather-station-form.js";

//@@viewOff:imports

//@@viewOn:constants
//@@viewOff:constants

//@@viewOn:css
const Css = {
  main: () => Config.Css.css({}),
};
//@@viewOff:css

//@@viewOn:helpers
//@@viewOff:helpers

let WeatherStationList = createVisualComponent({
  //@@viewOn:statics
  uu5Tag: Config.TAG + "WeatherStationList",
  //@@viewOff:statics

  //@@viewOn:propTypes
  propTypes: {},
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {},
  //@@viewOff:defaultProps

  render(props) {
    //@@viewOn:private
    const [viewType, setViewType] = useState("grid");
    const [selectedStation, setSelectedStation] = useState(null);
    const [stationToDelete, setStationToDelete] = useState(null);
    const tilesProviderRef = useRef();
    const screensize = useScreenSize();
    const permission = usePermission();
    const filtersF = [
      {
        key: "name",
        label: WeatherStationCfg.form.name,
        filterFn: (item, value) => {
          return item.data.name.toLowerCase().includes(value.toLowerCase());
        },
      },
      {
        key: "serialNumber",
        label: WeatherStationCfg.form.serialNumber,
        filterFn: (item, value) => {
          return item.data.serialNumber.toLowerCase().includes(value.toLowerCase());
        },
      },
      {
        key: "code",
        label: WeatherStationCfg.form.code,
        filterFn: (item, value) => {
          return item.data.code.toLowerCase().includes(value.toLowerCase());
        },
      },
      {
        key: "location",
        label: WeatherStationCfg.form.location,
        filterFn: (item, value) => {
          return item.data.location.toLowerCase().includes(value.toLowerCase());
        },
      },
    ];

    const weatherStationDataList = useDataList({
      handlerMap: {
        load: Calls.listWeatherStation,
        createItem: Calls.createWeatherStation,
      },
      itemHandlerMap: {
        updateItem: Calls.editWeatherStation,
        deleteItem: Calls.deleteWeatherStation,
      },
      initialDtoIn: {},
    });

    function getChild() {
      let child;
      switch (weatherStationDataList.state) {
        case "pendingNoData":
          child = <Uu5Elements.Pending />;
          break;
        case "pending":
        case "itemPending":
        case "error":
        case "readyNoData":
        case "ready":
          child = (
            <Uu5Tiles.ControllerProvider
              _ref={(opt) => (tilesProviderRef.current = opt)}
              data={weatherStationDataList.data || []}
              filters={filtersF}
              sorters={[
                {
                  key: "Name",
                  label: LSI.name,
                  ascending: true,
                  sorterFn: (NameA, NameB) => NameA.data.name.localeCompare(NameB.data.name),
                },
              ]}
            >
              {!!selectedStation && (
                <WeatherStationForm
                  createItem={weatherStationDataList.handlerMap.createItem}
                  selectedSubject={selectedStation}
                  setSelectedSubject={setSelectedStation}
                />
              )}
              {!!stationToDelete && (
                <Uu5Elements.Dialog
                  open={true}
                  onClose={() => setStationToDelete(null)}
                  actionList={[
                    {
                      children: <Lsi lsi={{ en: "Yes", cs: "Ano" }} />,
                      onClick: async () => {
                        try {
                          await stationToDelete.handlerMap.deleteItem({ id: stationToDelete.data.id });
                        } catch (err) {
                          alert(WeatherStationCfg.deleteConfirmationError.cs);
                        }
                      },
                      colorScheme: "negative",
                      significance: "highlighted",
                    },
                    {
                      children: <Lsi lsi={{ en: "No", cs: "Ne" }} />,
                      onClick: () => setStationToDelete(null),
                      significance: "distinct",
                    },
                  ]}
                  header={<Lsi lsi={WeatherStationCfg.deleteConfirmation} />}
                />
              )}
              <Uu5Tiles.FilterBar />
              <Uu5Tiles.InfoBar />
              {viewType === "grid" || ["xs", "s"].includes(screensize[0]) ? (
                <div className={Config.Css.css({ marginTop: 7 })}>
                  <Uu5Tiles.Grid
                    tileHeight={318}
                    tileMinWidth={200}
                    tileMaxWidth={400}
                    tileSpacing={16}
                    rowSpacing={-150}
                  >
                    {(props) => (
                      <WeatherStationListTitle
                        {...props}
                        setSelectedSubject={setSelectedStation}
                        setSubjectToDelete={setStationToDelete}
                      />
                    )}
                  </Uu5Tiles.Grid>
                </div>
              ) : (
                <Uu5Tiles.List
                  columns={[
                    {
                      key: "Name",
                      sorterKey: "Name",
                      header: <Lsi lsi={LSI.name} />,
                      cell: (cellProps) => {
                        return cellProps.data.data.name;
                      },
                    },
                    {
                      cell: (cellProps) => {
                        return (
                          <div>
                            <Uu5Elements.Button
                              icon={"mdi-pencil"}
                              size={"s"}
                              significance={"subdued"}
                              colorScheme={"blue"}
                              tooltip={LSI.update}
                              onClick={() => setSelectedStation(cellProps.data)}
                            />
                            <Uu5Elements.Button
                              icon={"mdi-trash-can-outline"}
                              size={"s"}
                              significance={"subdued"}
                              colorScheme={"red"}
                              tooltip={LSI.delete}
                              onClick={() => setStationToDelete(cellProps.data)}
                            />
                          </div>
                        );
                      },
                      width: 70,
                    },
                  ]}
                />
              )}
            </Uu5Tiles.ControllerProvider>
          );
          break;
        case "errorNoData":
          child = <Uu5Elements.HighlightedBox colorScheme={"negative"}>Error</Uu5Elements.HighlightedBox>;
          break;
        default:
          child = null;
      }
      return child;
    }
    //@@viewOff:private

    //@@viewOn:interface
    //@@viewOff:interface

    //@@viewOn:render
    const attrs = Utils.VisualComponent.getAttrs(props, Css.main());

    return (
      <Uu5Elements.Box {...attrs} colorScheme={"grey"}>
        <div className={Config.Css.css({ margin: "16px 32px" })}>
          <Uu5Elements.Block
            header={
              <Uu5Elements.Text category="story" segment="heading" type="h2">
                <Lsi lsi={WeatherStationCfg.header} />
              </Uu5Elements.Text>
            }
            card={"none"}
            actionList={[
              permission.station.canCreate() && {
                icon: "mdi-plus",
                children: <Lsi lsi={WeatherStationCfg.create} />,
                primary: true,
                onClick: () => setSelectedStation({}),
              },
              {
                icon: viewType === "grid" ? "mdi-view-headline" : "mdi-view-module",
                onClick: () => setViewType((currentViewType) => (currentViewType === "grid" ? "table" : "grid")),
              },
            ]}
            collapsible={false}
          >
            {getChild()}
          </Uu5Elements.Block>
        </div>
      </Uu5Elements.Box>
    );
    //@@viewOff:render
  },
});

WeatherStationList = withRoute(WeatherStationList, { authenticated: false });

//@@viewOn:exports
export { WeatherStationList };
export default WeatherStationList;
//@@viewOff:exports
