//@@viewOn:imports
import { Utils, createVisualComponent, Lsi, useDataList, useState, useCallback, useRoute } from "uu5g05";
import Uu5Elements from "uu5g05-elements";
import { withRoute } from "uu_plus4u5g02-app";
import Uu5Tiles from "uu5tilesg02";
import Calls from "calls";

import Config from "./config/config";
import WeatherStationCfg from "../config/weatherStation.js";
import LSI from "../config/lsi.js";

//@@viewOff:imports

//@@viewOn:constants
//@@viewOff:constants

//@@viewOn:css
const Css = {
  body: Config.Css.css({}),
};
//@@viewOff:css

//@@viewOn:helpers
const transformFilterToQuery = (activeFilters) => {
  const filterData = {};
  activeFilters.forEach((item) => {
    filterData[item.key] = item.value;
    if (item.key === "idWeatherStation" && item.items) {
      const station = item.items.find(({ value }) => value === item.value);
      if (station && station.label) {
        filterData.stationName = station.label.en;
      }
    }
  });

  return {
    stationName: filterData.stationName,
    idWeatherStation: filterData.idWeatherStation,
    timePeriod: filterData.granularity,
    timeInterval: {
      start: new Date(filterData.dateStart).getTime(),
      end: new Date(filterData.dateEnd).getTime(),
    },
  };
};
//@@viewOff:helpers

let WeatherStations = createVisualComponent({
  //@@viewOn:statics
  uu5Tag: Config.TAG + "WeatherStations",
  //@@viewOff:statics

  //@@viewOn:propTypes
  propTypes: {},
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {},
  //@@viewOff:defaultProps

  render(props) {
    //@@viewOn:private
    const [{ params }] = useRoute();

    const series = [
      {
        valueKey: "temperature",
        name: "temperature",
        colorSchema: "red",
      },
      {
        valueKey: "humidity",
        name: "humidity",
        colorSchema: "blue",
      },
    ];

    const weatherStationDataList = useDataList({
      handlerMap: {
        load: Calls.listWeatherStation,
      },
    });

    const filtersF = [
      {
        key: "idWeatherStation",
        label: WeatherStationCfg.dashboard.filter.idWeatherStation,
        type: "select",
        items: weatherStationDataList.data
          ? weatherStationDataList.data.map((item) => ({
              value: item.data.id,
              label: { en: `${item.data.name} (${item.data.code})`, cs: `${item.data.name} (${item.data.code})` },
            }))
          : [],
      },
      {
        key: "granularity",
        label: WeatherStationCfg.dashboard.filter.granularity,
        type: "select",
        items: [
          { value: "60", label: { cs: "1 minuta", en: "1 minute" } },
          { value: "300", label: { cs: "5 minut", en: "5 minutes" } },
          { value: "600", label: { cs: "10 minut", en: "10 minutes" } },
          { value: "1800", label: { cs: "30 minut", en: "30 minutes" } },
          { value: "3600", label: { cs: "1 hodina", en: "1 hour" } },
          { value: "86400", label: { cs: "1 den", en: "1 day" } },
        ],
      },
      {
        key: "dateStart",
        label: WeatherStationCfg.dashboard.filter.from,
        component: <UU5.Forms.DateTimePicker format="Y-mm-dd" />,
      },
      {
        key: "dateEnd",
        label: WeatherStationCfg.dashboard.filter.to,
        component: <UU5.Forms.DateTimePicker format="Y-mm-dd" />,
      },
    ];

    const [weatherStationName, setWeatherStationName] = useState(params ? params.name : "");
    const [filter] = useState([
      { key: "idWeatherStation", value: params ? params.id : "" },
      { key: "granularity", value: "600" },
      { key: "dateStart", value: new Date(new Date().setUTCHours(0, 0, 0, 0)).toUTCString() },
      { key: "dateEnd", value: new Date().toUTCString() },
    ]);
    const dataList = useDataList({
      handlerMap: {
        load: Calls.weatherStationData,
      },
      initialDtoIn: {
        ...transformFilterToQuery(filter),
      },
    });
    const handleFilterChange = useCallback(
      ({ activeFilters }) => {
        const filterData = transformFilterToQuery(activeFilters);
        setWeatherStationName(filterData.stationName);

        dataList.handlerMap.load(filterData);
      },
      [dataList.handlerMap, setWeatherStationName]
    );

    UU5.Environment.uu5DataMap = {
      data: dataList.data
        ? dataList.data.map((item) => ({
            label: UU5.Common.Tools.formatDate(new Date(item.data.ISOTime), "d. m. Y HH:MM"),
            temperature: item.data.temperature,
            humidity: item.data.humidity,
          }))
        : [],
      series,
    };
    //@@viewOff:private

    //@@viewOn:interface
    //@@viewOff:interface

    //@@viewOn:render
    const attrs = Utils.VisualComponent.getAttrs(props, Css.body);

    function getChild() {
      let child;
      switch (weatherStationDataList.state) {
        case "pendingNoData":
          child = <Uu5Elements.Pending />;
          break;
        case "pending":
        case "itemPending":
        case "error":
        case "readyNoData":
        case "ready":
          child = (
            <div {...attrs}>
              <Uu5Tiles.ControllerProvider
                initialActiveFilters={filter}
                onChangeFilters={handleFilterChange}
                data={[]}
                filters={filtersF}
                sorters={[]}
              >
                <Uu5Tiles.FilterBar />
              </Uu5Tiles.ControllerProvider>
              <UU5.Bricks.Section
                header={`<uu5string/><UuContentKit.Bricks.BlockInfo icon="fa-cloud" content="${weatherStationName}" />`}
                content={`<uu5string/>
      <UU5.SimpleChart.AreaChart
        data='<uu5data/>data'
        series='<uu5data/>series'
      />
      <UU5.SimpleChart.BarChart
        data='<uu5data/>data'
        series='<uu5data/>series'
      />
      `}
              />
            </div>
          );
          break;
        case "errorNoData":
          child = <Uu5Elements.HighlightedBox colorScheme={"negative"}>Error</Uu5Elements.HighlightedBox>;
          break;
        default:
          child = null;
      }
      return child;
    }

    return getChild();
    //@@viewOff:render
  },
});

WeatherStations = withRoute(WeatherStations, { authenticated: false });

//@@viewOn:exports
export { WeatherStations };
export default WeatherStations;
//@@viewOff:exports
