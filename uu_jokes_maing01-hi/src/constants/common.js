export const EQUIPMENT = {
  THERMOMETER: "lsi.constants.common.equipment.THERMOMETER",
  BAROMETER: "lsi.constants.common.equipment.BAROMETER",
};

export const DATA_TYPES = {
  TEMPERATURE: "lsi.constants.common.dataTypes.TEMPERATURE",
  PRESSURE: "lsi.constants.common.dataTypes.PRESSURE",
};
