export const EQUIPMENT = {
  THERMOMETER: "weatherStation.equipment.THERMOMETER",
  BAROMETER: "weatherStation.equipment.BAROMETER",
};

export const DATA_TYPES = {
  THERMOMETER: "weatherStation.dataTypes.TEMPERATURE",
  BAROMETER: "weatherStation.dataTypes.PRESSURE",
};
