import { Environment } from "uu5g05";
import Plus4U5 from "uu_plus4u5g02";

// the base URI of calls for development / staging environments can be configured in *-hi/env/development.json
// (or <stagingEnv>.json), e.g.:
//   "uu5Environment": {
//     "callsBaseUri": "http://localhost:8080/vnd-app/awid"
//   }
const CALLS_BASE_URI = (
  (process.env.NODE_ENV !== "production" ? Environment.get("callsBaseUri") : null) || Environment.appBaseUri
).replace(/\/*$/, "/");

const Calls = {
  async call(method, url, dtoIn, clientOptions) {
    const response = await Plus4U5.Utils.AppClient[method](url, dtoIn, clientOptions);
    return response.data;
  },

  permission(dtoIn) {
    const commandUri = Calls.getCommandUri("permission");
    return Calls.call("get", commandUri, dtoIn);
  },
  listWeatherStation(dtoIn) {
    const commandUri = Calls.getCommandUri("weatherStation/list");
    return Calls.call("get", commandUri, dtoIn);
  },
  createWeatherStation(dtoIn) {
    const commandUri = Calls.getCommandUri("weatherStation/add");
    return Calls.call("post", commandUri, dtoIn);
  },
  editWeatherStation(dtoIn) {
    const commandUri = Calls.getCommandUri("weatherStation/update");
    return Calls.call("post", commandUri, dtoIn);
  },
  deleteWeatherStation(dtoIn) {
    const commandUri = Calls.getCommandUri("weatherStation/delete");
    return Calls.call("post", commandUri, dtoIn);
  },
  weatherStationData(dtoIn) {
    const commandUri = Calls.getCommandUri("weatherStationData/list");
    return Calls.call("get", commandUri, dtoIn);
  },

  // // example for mock calls
  // loadDemoContent(dtoIn) {
  //   const commandUri = Calls.getCommandUri("loadDemoContent");
  //   return Calls.call("get", commandUri, dtoIn);
  // },

  loadIdentityProfiles() {
    const commandUri = Calls.getCommandUri("sys/uuAppWorkspace/initUve");
    return Calls.call("get", commandUri, {});
  },

  initWorkspace(dtoInData) {
    const commandUri = Calls.getCommandUri("sys/uuAppWorkspace/init");
    return Calls.call("post", commandUri, dtoInData);
  },

  getWorkspace() {
    const commandUri = Calls.getCommandUri("sys/uuAppWorkspace/get");
    return Calls.call("get", commandUri, {});
  },

  async initAndGetWorkspace(dtoInData) {
    await Calls.initWorkspace(dtoInData);
    return await Calls.getWorkspace();
  },

  getCommandUri(useCase) {
    return CALLS_BASE_URI + useCase.replace(/^\/+/, "");
  },
};

export default Calls;
