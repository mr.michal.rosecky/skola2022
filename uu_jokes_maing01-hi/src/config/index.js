import About from "./about";
import Home from "./home";
import Lsi from "./lsi";

export default {
  home: { ...Home },
  lsi: { ...Lsi },
  about: { ...About },
};
