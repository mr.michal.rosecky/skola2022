import Uu5Elements from "uu5g05-elements";

const Home = {
  welcome: {
    cs: "Vítejte na stránkách Meteostanice.",
    en: "Welcome to our Weather station website.",
  },

  header: {
    cs: "",
    en: " ",
  },
  info: {
    study: {
      cs: "uuMeteostation je aplikace, která spojuje lidi po celém světě. Ti mohou k aplikaci připojit své vlastní meteostanice a dát zbytku světa vědět, jaká je teplota a vlhkost u nich doma. Tyto údaje si mohou dát i do soukromí a užívat si grafické znázornění svého prostředí v soukromí. ",
      en: "uuMeteostation is an application that connects people around the globe. They can connect their own meteostations to the application and let the rest of the world know what their home temperature and humidity feels like. They can also put these data on private, enjoying the graphical representation of their environment in private.",
    },
  },
  fullTime: {
    header: { cs: "", en: "" },
    body: {
      cs: "• Správa meteostanic (přidávání, úpravy, mazání)",
      en: "• Manage your meteostations (add, edit, delete)",
    },
  },
  partTime: {
    header: { cs: "", en: "" },
    body: { cs: "• Sdílení dat z meteostanice s okolím", en: "• Share your meteostation data with your surroundings" },
  },
  online: {
    header: { cs: "", en: "" },
    body: { cs: "• Vizualizace meteostaničních dat za určitá období", en: "• Visualize your meteostation data for certain periods" },
  },

  videa: {
    header: { cs: "Pro více informací se neváhejte podívat na naše instruktážní video níže!", en: "For more information, don't hesitate to check out our instructional video below!" },
  },
};

export { Home };
export default Home;
