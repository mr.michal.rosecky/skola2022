import Uu5Elements from "uu5g05-elements";

const WeatherStation = {
  header: {
    cs: "Data Meteostanice",
    en: "Weatherstation data",
  },
  dashboard: {
    cs: "Meteostanice",
    en: "Weatherstations",
  },
  temperature: {
    cs: "Teplota",
    en: "Temperature",
  },
  humidity: {
    cs: "Vlhkost",
    en: "Humidity",
  },
  tile: {
    open: { cs: "Otevřít", en: "open" },
    update: {
      cs: "Editovat stanici",
      en: "Edit station",
    },
    delete: {
      cs: "Smazat stanici",
      en: "Delete station",
    },
  },
  form: {
    name: { cs: "Název", en: "Name" },
    code: { cs: "Kód", en: "Code" },
    location: { cs: "Umístění", en: "Location" },
    equipment: { cs: "Typ zařízeni", en: "Equipment" },
    dataTypes: { cs: "Datový typ", en: "Data types" },
    serialNumber: { cs: "Seriové číslo", en: "Serial number" },
    isPublic: { cs: "Veřejné", en: "Public" },
  },
  dashboard: {
    filter: {
      idWeatherStation: { cs: "Meteostanice", en: "Weather station" },
      granularity: { cs: "Granularita", en: "Granularity" },
      from: { cs: "Od", en: "From" },
      to: { cs: "Do", en: "To" },
    },
  },
  deleteConfirmation: {
    cs: "Opravdu chcete smazat tuto stanici?",
    en: "Do you really want to delete this station?",
  },
  deleteConfirmationError: {
    cs: "Nastala neocekavana chyba",
    en: "There was an unexpected error",
  },
  create: {
    cs: "Vytvořit meteostanici",
    en: "Create meteostation",
  },
  update: {
    cs: "Upravit meteostanici",
    en: "Update meteostation",
  },
};

export { WeatherStation };
export default WeatherStation;
