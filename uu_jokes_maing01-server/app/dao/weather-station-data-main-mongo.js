"use strict";
const { UuObjectDao } = require("uu_appg01_server").ObjectStore;
const ObjectId = require("mongodb").ObjectID;

class WeatherStationDataMainMongo extends UuObjectDao {
  async createSchema() {
    await super.createIndex({ awid: 1, weatherStationId: 1 }, { unique: true });
  }

  /*
  full example
  .aggregate(
    [{
        "$project": {
            y: { "$year": "$ISOTime" },
            m: { "$month": "$ISOTime" },
            d: { "$dayOfMonth": "$ISOTime" },
            h: { $hour: "$ISOTime" },
            min: { $minute: "$ISOTime" },
            interval: {
              $subtract: [{ $minute: "$ISOTime" }, { $mod: [{ $minute: "$ISOTime" }, 30] }],
            },
            temperature: "$temperature",
        }
    },
    {
        "$group": {
            "_id": { "year":"$y","month":"$m","day":"$d","hour":"$h", "minute":"$interval" },
            temperature: { $avg: "$temperature" },
        }
    }])
  */

  async listOfData(awid, uuObject) {
    const timePeriod = Number(uuObject.timePeriod);
    const project = {
      y: { $year: "$ISOTime" },
      m: { $month: "$ISOTime" },
      d: { $dayOfMonth: "$ISOTime" },
      h: { $hour: "$ISOTime" },
    };
    const groupId = { year: "$y", month: "$m", day: "$d" };

    if (timePeriod === 60) {
      groupId.hour = "$h";
      groupId.minute = "$interval";
      project.interval = { $minute: "$ISOTime" };
    } else if (timePeriod > 60 && timePeriod < 3600) {
      groupId.hour = "$h";
      groupId.minute = "$interval";
      project.interval = {
        $subtract: [{ $minute: "$ISOTime" }, { $mod: [{ $minute: "$ISOTime" }, timePeriod / 60] }],
      };
    } else if (timePeriod === 3600) {
      groupId.hour = "$h";
    }

    return await await super.aggregate([
      {
        $match: {
          $and: [
            { awid },
            { weatherStationId: ObjectId(uuObject.idWeatherStation) },
            { ISOTime: { $gt: new Date(uuObject.timeInterval.start), $lt: new Date(uuObject.timeInterval.end) } },
          ],
        },
      },
      {
        $project: {
          ...project,
          temperature: "$temperature",
          humidity: "$humidity",
          ISOTime: "$ISOTime",
        },
      },
      {
        $group: {
          _id: groupId,
          temperature: { $avg: "$temperature" },
          humidity: { $avg: "$humidity" },
          ISOTime: { $first: "$ISOTime" },
        },
      },
      { $sort: { ISOTime: 1 } },
    ]);
  }

  async upload(awid, weatherStationId, uuObject) {
    const data = uuObject.data.map((item) => ({
      ...item,
      ISOTime: new Date(item.ISOTime),
      weatherStationId,
      awid,
    }));

    return await super.insertMany(data, false);
  }

  async removeAll(uuObject) {
    let filter = {
      awid: uuObject.awid,
      weatherStationId: uuObject.weatherStationId,
    };
    return await super.deleteOne(filter);
  }
}

module.exports = WeatherStationDataMainMongo;
