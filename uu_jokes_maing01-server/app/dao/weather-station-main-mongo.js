"use strict";
const { UuObjectDao } = require("uu_appg01_server").ObjectStore;

class WeatherStationMainMongo extends UuObjectDao {
  async createSchema() {
    await super.createIndex({ awid: 1 }, { unique: true });
  }

  async create(uuObject) {
    return await super.insertOne(uuObject);
  }

  async get(awid, id) {
    let filter = {
      awid: awid,
      _id: id,
    };

    return await super.findOne(filter);
  }

  async getFirst(awid, uuObject) {
    let filter = {
      ...uuObject,
      awid: awid,
    };
    return await super.findOne(filter);
  }

  async update(uuObject) {
    let filter = {
      awid: uuObject.awid,
      _id: uuObject.id,
    };
    return await super.findOneAndUpdate(filter, uuObject, "NONE");
  }

  async remove(uuObject) {
    let filter = {
      awid: uuObject.awid,
      id: uuObject.id,
    };
    return await super.deleteOne(filter);
  }

  async list(awid, pageInfo, filterMap = {}, sortMap = {}) {
    console.log("filterMap: ", filterMap);
    if (filterMap.code) {
      filterMap.code = `/${filterMap.code}/`;
    }
    if (filterMap.name) {
      filterMap.name = `/${filterMap.name}/`;
    }
    if (filterMap.serialNumber) {
      filterMap.serialNumber = `/${filterMap.serialNumber}/`;
    }

    if (filterMap.states) {
      filterMap.states = { $in: filterMap.states };
    }

    return await super.find({ awid, ...filterMap }, pageInfo, sortMap);
  }
}

module.exports = WeatherStationMainMongo;
