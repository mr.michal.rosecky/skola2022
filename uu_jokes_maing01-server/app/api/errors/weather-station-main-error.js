"use strict";

const WeatherStationMainUseCaseError = require("./weather-station-main-use-case-error.js");

const Init = {
  UC_CODE: `${WeatherStationMainUseCaseError.ERROR_PREFIX}init/`,

  InvalidDtoIn: class extends WeatherStationMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Init.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },

  NotFound: class extends WeatherStationMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}notFound`;
      this.message = "Weather station not found";
    }
  },
};

const Create = {
  UC_CODE: `${WeatherStationMainUseCaseError.ERROR_PREFIX}create/`,

  AlreadyExists: class extends WeatherStationMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}alreadyExists`;
      this.message = "This serial number already exists.";
    }
  },

  WeatherStationDaoCreateFailed: class extends WeatherStationMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}weatherStationDaoCreateFailed`;
      this.message = "Create weather station by dao failed.";
    }
  },
};

const Delete = {
  UC_CODE: `${WeatherStationMainUseCaseError.ERROR_PREFIX}delete/`,

  WeatherStationDaoDeleteFailed: class extends WeatherStationMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Delete.UC_CODE}weatherStationDaoDeleteFailed`;
      this.message = "Delete weather station by dao failed.";
    }
  },

  Forbidden: class extends WeatherStationMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Delete.UC_CODE}forbidden`;
      this.message = "Doesn't have permission";
    }
  },
};

const List = {
  UC_CODE: `${WeatherStationMainUseCaseError.ERROR_PREFIX}list/`,
};

module.exports = {
  Init,
  Create,
  Delete,
  List
};
