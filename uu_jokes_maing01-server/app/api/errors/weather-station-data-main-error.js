"use strict";
const WeatherStationDataMainUseCaseError = require("./weather-station-data-main-use-case-error.js");

const Init = {
  UC_CODE: `${WeatherStationDataMainUseCaseError.ERROR_PREFIX}init/`,

  InvalidDtoIn: class extends WeatherStationDataMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Init.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },

  NotFound: class extends WeatherStationDataMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}notFound`;
      this.message = "Weather station data not found";
    }
  },
};

const List = {
  UC_CODE: `${WeatherStationDataMainUseCaseError.ERROR_PREFIX}get/`,
  
  NotFound: class extends WeatherStationDataMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${List.UC_CODE}notFound`;
      this.message = "Weather station  not found";
    }
  },

  Forbidden: class extends WeatherStationDataMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${List.UC_CODE}forbidden`;
      this.message = "Doesn't have permission";
    }
  },
  Limit: class extends WeatherStationDataMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${List.UC_CODE}limit`;
      this.message = "You have exceeded the data download limit";
    }
  },
};

const Upload = {
  UC_CODE: `${WeatherStationDataMainUseCaseError.ERROR_PREFIX}upload/`,

  NotFound: class extends WeatherStationDataMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Upload.UC_CODE}notFound`;
      this.message = "Weather station  not found";
    }
  },

  WeatherStationDataDaoUploadFailed: class extends WeatherStationDataMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Upload.UC_CODE}weatherStationDataDaoUploadFailed`;
      this.message = "Upload weather station data by dao failed.";
    }
  },
};

module.exports = {
  Init,
  List,
  Upload,
};
