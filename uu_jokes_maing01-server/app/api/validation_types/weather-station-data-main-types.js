const listWeatherDataDtoInType = shape({
  idWeatherStation: mongoId().isRequired(),
  timePeriod: integer().isRequired(),
  timeInterval: shape({
    start: integer().isRequired(),
    end: integer().isRequired(),
  }).isRequired(),
});

const uploadWeatherStationDtoInType = shape({
  deviceSerialNumber: string(18).isRequired(),
  data: array(
    shape({
      ISOTime: dateTime().isRequired(),
      temperature: number(),
      humidity: number(),
    })
  ),
});
