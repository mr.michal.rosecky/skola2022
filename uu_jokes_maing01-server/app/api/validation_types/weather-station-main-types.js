const addWeatherStationDtoInType = shape({
  name: string(50).isRequired(),
  code: string(10).isRequired(),
  location: string(50).isRequired(),
  equipment: array(oneOf(["THERMOMETER", "BAROMETER"]), 2),
  dataTypes: array(oneOf(["TEMPERATURE", "PRESSURE"]), 2),
  serialNumber: string(18).isRequired(),
  isPublic: boolean(),
});

const editWeatherStationDtoInType = shape({
  id: mongoId().isRequired(),
  name: string(50).isRequired(),
  code: string(10).isRequired(),
  location: string(50).isRequired(),
  equipment: array(oneOf(["THERMOMETER", "BAROMETER"]), 2),
  dataTypes: array(oneOf(["TEMPERATURE", "PRESSURE"]), 2),
  serialNumber: string(18).isRequired(),
  isPublic: boolean(),
});

const deleteWeatherStationDtoInType = shape({
  id: mongoId().isRequired(),
});

const getWeatherStationDtoInType = shape({
  id: mongoId().isRequired(),
});

const listWeatherStationDtoInType = shape({
  sortMap: shape({
    name: oneOf(["asc", "desc"]),
    createAt: oneOf(["asc", "desc"]),
    location: oneOf(["asc", "desc"]),
  }),
  pageInfo: shape({
    pageIndex: integer(),
    pageSize: integer(),
  }),
  filterMap: shape({
    code: string(10),
    name: string(50).isRequired(),
    states: array(oneOf(["created", "blocked", "active", "disabled"]), 4),
    serialNumber: string(50),
  }),
});
