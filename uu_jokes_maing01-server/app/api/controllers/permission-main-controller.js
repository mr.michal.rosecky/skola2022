"use strict";
const PermissionMainAbl = require("../../abl/permission-main-abl.js");

class PermissionMainController {

  permission(ucEnv) {
    return PermissionMainAbl.permission(ucEnv.getUri(), ucEnv.getSession(), ucEnv.getAuthorizationResult());
  }

}

module.exports = new PermissionMainController();
