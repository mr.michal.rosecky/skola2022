"use strict";
const WeatherStationMainAbl = require("../../abl/weather-station-main-abl.js");

class WeatherStationMainController {
  add(ucEnv) {
    return WeatherStationMainAbl.addWeatherStation(
      ucEnv.getUri().getAwid(),
      ucEnv.getDtoIn(),
      ucEnv.getAuthorizationResult()
    );
  }

  update(ucEnv) {
    return WeatherStationMainAbl.updateWeatherStation(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }

  delete(ucEnv) {
    return WeatherStationMainAbl.deleteWeatherStation(
      ucEnv.getUri().getAwid(),
      ucEnv.getDtoIn(),
      ucEnv.getAuthorizationResult()
    );
  }

  get(ucEnv) {
    return WeatherStationMainAbl.getWeatherStations(
      ucEnv.getUri().getAwid(),
      ucEnv.getDtoIn(),
      ucEnv.getAuthorizationResult()
    );
  }

  list(ucEnv) {
    return WeatherStationMainAbl.listWeatherStations(
      ucEnv.getUri().getAwid(),
      ucEnv.getDtoIn(),
      ucEnv.getAuthorizationResult()
    );
  }
}

module.exports = new WeatherStationMainController();
