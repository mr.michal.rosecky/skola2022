"use strict";
const WeatherStationDataMainAbl = require("../../abl/weather-station-data-main-abl.js");

class WeatherStationDataMainController {
  list(ucEnv) {
    return WeatherStationDataMainAbl.listWeatherStationData(
      ucEnv.getUri().getAwid(),
      ucEnv.getDtoIn(),
      ucEnv.getAuthorizationResult()
    );
  }

  upload(ucEnv) {
    return WeatherStationDataMainAbl.uploadWeatherStationData(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }
}

module.exports = new WeatherStationDataMainController();
