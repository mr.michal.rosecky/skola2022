"use strict";
const Path = require("path");
const { Validator } = require("uu_appg01_server").Validation;
const { DaoFactory, ObjectStoreError } = require("uu_appg01_server").ObjectStore;
const { ValidationHelper } = require("uu_appg01_server").AppServer;
const Errors = require("../api/errors/weather-station-main-error.js");

const WARNINGS = {
  addUnsupportedKeys: {
    code: `${Errors.Create.UC_CODE}unsupportedKeys`,
  },
  deleteUnsupportedKeys: {
    code: `${Errors.Delete.UC_CODE}unsupportedKeys`,
  },
  listUnsupportedKeys: {
    code: `${Errors.List.UC_CODE}unsupportedKeys`,
  },
};

class WeatherStationMainAbl {
  constructor() {
    this.validator = Validator.load();
    this.weatherStationDao = DaoFactory.getDao("weatherStation");
    this.weatherStationDataDao = DaoFactory.getDao("weatherStationData");
  }

  async addWeatherStation(awid, dtoIn, authorizationResult) {
    // HDS 1.1
    let validationResult = this.validator.validate("addWeatherStationDtoInType", dtoIn);

    // HDS 1.2, 1.3 // A1, A2
    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.addUnsupportedKeys.code,
      Errors.Init.InvalidDtoIn
    );

    let existedWeatherStation = await this.weatherStationDao.getFirst(awid, { serialNumber: dtoIn.serialNumber });
    // HDS 1.4
    if (existedWeatherStation) {
      throw new Errors.Create.AlreadyExists({ uuAppErrorMap }, { serialNumber: dtoIn.serialNumber });
    }

    // HDS 2
    dtoIn.awid = awid;
    dtoIn.uuIdentity = authorizationResult._uuIdentity;
    let dtoOut;
    try {
      dtoOut = await this.weatherStationDao.create(dtoIn);
    } catch (e) {
      if (e instanceof ObjectStoreError) {
        // A3
        throw new Errors.Create.WeatherStationDaoCreateFailed({ uuAppErrorMap }, e);
      }
      throw e;
    }

    // HDS 3
    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }

  async updateWeatherStation(awid, dtoIn) {
    // HDS 1.1
    let validationResult = this.validator.validate("editWeatherStationDtoInType", dtoIn);

    // HDS 1.2, 1.3 // A1, A2
    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.addUnsupportedKeys.code,
      Errors.Init.InvalidDtoIn
    );

    const existedWeatherStation = await this.weatherStationDao.get(awid, dtoIn.id);
    if (!existedWeatherStation) {
      throw new Errors.Init.NotFound({ uuAppErrorMap });
    }

    // HDS 2
    dtoIn.awid = awid;
    let dtoOut;
    try {
      dtoOut = await this.weatherStationDao.update(dtoIn);
    } catch (e) {
      if (e instanceof ObjectStoreError) {
        // A3
        throw new Errors.Create.WeatherStationDaoCreateFailed({ uuAppErrorMap }, e);
      }
      throw e;
    }

    // HDS 3
    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }

  async deleteWeatherStation(awid, dtoIn, authorizationResult) {
    let validationResult = this.validator.validate("deleteWeatherStationDtoInType", dtoIn);

    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.addUnsupportedKeys.code,
      Errors.Init.InvalidDtoIn
    );

    dtoIn.awid = awid;
    let dtoOut;
    try {
      const existedWeatherStation = await this.weatherStationDao.get(awid, dtoIn.id);
      if (!existedWeatherStation) {
        throw new Errors.Init.NotFound({ uuAppErrorMap });
      }
      if (
        authorizationResult._authorizedProfiles.includes("User") &&
        existedWeatherStation.uuIdentity !== authorizationResult._uuIdentity
      ) {
        throw new Errors.Delete.Forbidden({ uuAppErrorMap });
      }

      await this.weatherStationDao.remove(dtoIn);
      await this.weatherStationDataDao.removeAll({ ...dtoIn, weatherStationId: dtoIn.id });
      dtoOut = {};
    } catch (e) {
      if (e instanceof ObjectStoreError) {
        throw new Errors.Delete.WeatherStationDaoDeleteFailed({ uuAppErrorMap }, e);
      }
      throw e;
    }

    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }

  async getWeatherStations(awid, dtoIn, authorizationResult) {
    let validationResult = this.validator.validate("getWeatherStationDtoInType", dtoIn);

    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.listUnsupportedKeys.code,
      Errors.Init.InvalidDtoIn
    );

    if (authorizationResult._uuIdentity === "0-0" || authorizationResult._authorizedProfiles.includes("Public")) {
      dtoIn.isPublic = true;
    } else if (authorizationResult._authorizedProfiles.includes("User")) {
      dtoIn.uuIdentity = authorizationResult._uuIdentity;
    }

    let dtoOut = await this.weatherStationDao.getFirst(awid, dtoIn);

    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }

  async listWeatherStations(awid, dtoIn, authorizationResult) {
    let validationResult = this.validator.validate("listWeatherStationDtoInType", dtoIn);

    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.listUnsupportedKeys.code,
      Errors.Init.InvalidDtoIn
    );

    if (!dtoIn.filterMap) {
      dtoIn.filterMap = {};
    }
    if (authorizationResult._uuIdentity === "0-0" || authorizationResult._authorizedProfiles.includes("Public")) {
      dtoIn.filterMap.isPublic = true;
    } else if (authorizationResult._authorizedProfiles.includes("User")) {
      dtoIn.filterMap.isPublic = true;
      dtoIn.filterMap.uuIdentity = authorizationResult._uuIdentity;
    }

    let dtoOut = await this.weatherStationDao.list(awid, dtoIn.pageInfo, dtoIn.filterMap, dtoIn.sortMap);

    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }
}

module.exports = new WeatherStationMainAbl();
