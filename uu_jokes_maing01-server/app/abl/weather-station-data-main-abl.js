"use strict";
const Path = require("path");
const { Validator } = require("uu_appg01_server").Validation;
const { DaoFactory, ObjectStoreError } = require("uu_appg01_server").ObjectStore;
const { ValidationHelper } = require("uu_appg01_server").AppServer;
const Errors = require("../api/errors/weather-station-data-main-error.js");

const WARNINGS = {
  getUnsupportedKeys: {
    code: `${Errors.List.UC_CODE}unsupportedKeys`,
  },
  uploadUnsupportedKeys: {
    code: `${Errors.Upload.UC_CODE}unsupportedKeys`,
  },
};

class WeatherStationDataMainAbl {
  constructor() {
    this.validator = Validator.load();
    this.weatherStationDataDao = DaoFactory.getDao("weatherStationData");
    this.weatherStationDao = DaoFactory.getDao("weatherStation");
  }

  async listWeatherStationData(awid, dtoIn, authorizationResult) {
    let validationResult = this.validator.validate("listWeatherDataDtoInType", dtoIn);

    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.getUnsupportedKeys.code,
      Errors.Init.InvalidDtoIn
    );

    let dtoOut;
    try {
      const weatherStation = await this.weatherStationDao.get(awid, dtoIn.idWeatherStation);
      if (!weatherStation) {
        throw new Errors.List.NotFound({ uuAppErrorMap }, { id: dtoIn.idWeatherStation });
      }
      if (
        (authorizationResult._uuIdentity === "0-0" || authorizationResult._authorizedProfiles.includes("Public")) &&
        weatherStation.isPublic == false
      ) {
        throw new Errors.List.Forbidden({ uuAppErrorMap }, { id: dtoIn.idWeatherStation });
      }
      if (
        authorizationResult._authorizedProfiles.includes("User") &&
        dtoIn.uuIdentity !== authorizationResult._uuIdentity
      ) {
        throw new Errors.List.Forbidden({ uuAppErrorMap }, { id: dtoIn.idWeatherStation });
      }

      const start = new Date(dtoIn.timeInterval.start);
      const end = new Date(dtoIn.timeInterval.end);
      let diffTime = Math.abs(start - end);
      if (diffTime > 32140800000) {
        diffTime = 32140800000;
        dtoIn.timeInterval.start = end - 32140800000;
        // vypsat warning, ze byl zmenen datum
      }
      const allowedCombinations = [
        // 1 den 86400000
        // 1h a 1m
        [3600000, 60, 300],
        // 5h a 5m
        [18000000, 300, 600],
        // 1den a 10m
        [86400000, 600, 1800],
        // 3dny a 30m
        [259200000, 1800, 3600],
        // 7dni a 1h
        [604800000, 3600, 86400],
        // 372dni a 1den
        [32140800000, 86400, 86400],
      ];

      allowedCombinations.forEach((combination) => {
        if (diffTime > combination[0]) {
          if (dtoIn.timePeriod <= combination[1]) {
            dtoIn.timePeriod = combination[2];
            // vypsat warning, ze byl zmenena granulita
          }
        }
      });

      dtoIn.awid = awid;
      dtoOut = await this.weatherStationDataDao.listOfData(awid, dtoIn);
    } catch (e) {
      if (e instanceof ObjectStoreError) {
        // A3
        throw new Errors.Upload.WeatherStationDataDaoUploadFailed({ uuAppErrorMap }, e);
      }
      throw e;
    }

    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }

  async uploadWeatherStationData(awid, dtoIn) {
    let validationResult = this.validator.validate("uploadWeatherStationDtoInType", dtoIn);

    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.getUnsupportedKeys.code,
      Errors.Init.InvalidDtoIn
    );
    let dtoOut;

    try {
      const weatherStation = await this.weatherStationDao.getFirst(awid, {
        serialNumber: dtoIn.deviceSerialNumber,
      });
      if (!weatherStation) {
        throw new Errors.Upload.NotFound({ uuAppErrorMap }, { serialNumber: dtoIn.serialNumber });
      }

      dtoOut = await this.weatherStationDataDao.upload(awid, weatherStation.id, dtoIn);
    } catch (e) {
      if (e instanceof ObjectStoreError) {
        // A3
        throw new Errors.Upload.WeatherStationDataDaoUploadFailed({ uuAppErrorMap }, e);
      }
      throw e;
    }

    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }
}

module.exports = new WeatherStationDataMainAbl();
