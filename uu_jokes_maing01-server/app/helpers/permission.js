const processValidationAcl = (profileList = []) => {
  const isAuthority = profileList.includes("Authorities");
  const isExecutive = profileList.includes("Executives");
  const isAwidLicenseOwner = profileList.includes("AwidLicenseOwner");

  function isOwner(entity) {
    return identity?.uuIdentity === entity.uuIdentity;
  }

  const subject = {
    canCreate: () => isAuthority || isExecutive,
    canUpdate: (entity) => isAuthority || (isExecutive && isOwner(entity)),
    canDelete: (entity) => isAuthority || (isExecutive && isOwner(entity)),
    canUpdateContent: (entity) => isAuthority || (isExecutive && isOwner(entity)),
    canShowList: () => isAuthority || isExecutive,
  };

  const studyProgram = {
    canCreate: () => isAuthority || isExecutive,
    canUpdate: (entity) => isAuthority || (isExecutive && isOwner(entity)),
    canDelete: (entity) => isAuthority || (isExecutive && isOwner(entity)),
    canShowList: () => isAuthority || isExecutive,
  };

  return {
    subject,
    studyProgram,
  };
};

module.exports = {
  processValidationAcl,
};
